package com.pegoopik.speculation.messenger;

import com.pegoopik.speculation.messenger.utils.SpecSound;
import lombok.SneakyThrows;

public class LocalSoundMessagingChecker {

    private static Long DELAY = 5000L;
    private static int COUNT = 20;

    @SneakyThrows
    public static void main(String[] args) {
        for(int i = 0; i<COUNT; i++) {
            double random = Math.random();
            if (random < 0.2D) {
                SpecSound.playSound("/home/pegoopik/Загрузки/pause3.wav");
            } else if (random < 0.4D) {
                SpecSound.playSound("/home/pegoopik/Загрузки/pause4.wav");
            } else if (random < 0.6D) {
                SpecSound.playSound("/home/pegoopik/Загрузки/20_Guitar in Jazz.wav");
            } else if (random < 0.8D) {
                SpecSound.playSound("/home/pegoopik/Загрузки/3285 (2).wav");
            }
            Thread.sleep(DELAY);
        }
    }

}
