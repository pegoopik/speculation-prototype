package com.pegoopik.speculation.messenger.service;

public interface SpecMessagingServiceSimple {

    void payMessage();

    void soldMessage();

    void errorMessage();

}
