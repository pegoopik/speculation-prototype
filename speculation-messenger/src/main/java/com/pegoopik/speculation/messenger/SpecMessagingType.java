package com.pegoopik.speculation.messenger;

public enum SpecMessagingType {

    SMS,
    SLACK,
    LOCAL_RING,
    EMAIL,

}
