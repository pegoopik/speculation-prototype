package com.pegoopik.speculation.course.monitor.service.pojo;

import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CoursePaySold {
    private Double pay;
    private Double sold;
}
