package com.pegoopik.speculation.course.monitor.service.impl;

import com.pegoopik.speculation.course.monitor.service.CurrentCourseService;
import com.pegoopik.speculation.course.monitor.service.pojo.CoursePaySold;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class CurrentCourseServiceImpl implements CurrentCourseService {

    @Override
    public double getCurrentCourse(String from, String to) {
        return getCurrentCourseStatic(from, to);
    }

    @SneakyThrows
    public static double getCurrentCourseStatic(String from, String to) {
        boolean needInvert = false;
        Document document;
        try {
            String courseUrl = getAlpariCurseUrl(from, to);
            document = Jsoup.connect(courseUrl)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
                    .get();
        } catch (Exception e) {
            needInvert = true;
            String courseUrl = getAlpariCurseUrl(to, from);
            document = Jsoup.connect(courseUrl)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
                    .get();
        }
        //Document document = Jsoup.connect("https://www.google.com/search?q=%D0%BA%D1%83%D1%80%D1%81+%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0").get();
        String stringCourse =
                document
                        .select("div.markets-forex-item__instrument-price-left")
                        .first()
                        .childNode(0)
                        .toString()
                        .trim();
        double course = Double.parseDouble(stringCourse);
        return needInvert ? 1D/course : course;
    }

    @SneakyThrows
    public static CoursePaySold getCurrentCoursePaySoldStatic(String from, String to) {
        boolean needInvert = false;
        Document document;
        try {
            String courseUrl = getAlpariCurseUrl(from, to);
            document = Jsoup.connect(courseUrl)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
                    .get();
        } catch (Exception e) {
            needInvert = true;
            String courseUrl = getAlpariCurseUrl(to, from);
            document = Jsoup.connect(courseUrl)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
                    .get();
        }
        //Document document = Jsoup.connect("https://www.google.com/search?q=%D0%BA%D1%83%D1%80%D1%81+%D0%B4%D0%BE%D0%BB%D0%BB%D0%B0%D1%80%D0%B0").get();
        String stringCourse = document.select("li.markets-forex-item__instrument-others-item")
                .get(2).getAllElements().get(2).childNode(0).toString().trim();
        String[] courses = stringCourse.split("/");
//        double course = Double.parseDouble(stringCourse);
        return new CoursePaySold(Double.parseDouble(courses[0].trim()), Double.parseDouble(courses[1].trim()));
//        return needInvert ? 1D/course : course;
    }


    private static String getAlpariCurseUrl(String from, String to) {
        return "https://alpari.com/ru/markets/forex/" + from + "-" + to + "/";
    }

}
