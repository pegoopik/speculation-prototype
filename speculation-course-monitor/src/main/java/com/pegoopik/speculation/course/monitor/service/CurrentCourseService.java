package com.pegoopik.speculation.course.monitor.service;

public interface CurrentCourseService {

    double getCurrentCourse(String from, String to);

}
