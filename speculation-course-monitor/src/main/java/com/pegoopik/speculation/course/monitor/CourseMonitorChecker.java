package com.pegoopik.speculation.course.monitor;

import com.pegoopik.speculation.course.monitor.service.impl.CurrentCourseServiceImpl;
import com.pegoopik.speculation.course.monitor.service.pojo.CoursePaySold;
import lombok.SneakyThrows;

public class CourseMonitorChecker {

    private static int COUNT_EXECUTE = 100;
    private static long DELAY_EXECUTE_MS = 300;

    @SneakyThrows
    public static void main(String[] args) {
        System.out.println("Display current courses: ");
        for (int dummy = 0; dummy < COUNT_EXECUTE; dummy ++) {
            CoursePaySold currentCourse = CurrentCourseServiceImpl.getCurrentCoursePaySoldStatic("usd", "rub");
            System.out.println("current course: " + currentCourse);
            Thread.sleep(DELAY_EXECUTE_MS);
        }
//        for (int dummy = 0; dummy < COUNT_EXECUTE; dummy ++) {
//            double currentCourse = CurrentCourseServiceImpl.getCurrentCourseStatic("usd", "rub");
//            System.out.println("current course: " + currentCourse);
//            Thread.sleep(DELAY_EXECUTE_MS);
//        }
    }

}
