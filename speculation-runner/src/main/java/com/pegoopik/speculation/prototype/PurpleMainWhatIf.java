package com.pegoopik.speculation.prototype;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.pegoopik.speculation.prototype.PurpleConsts.*;

public class PurpleMainWhatIf {

    private static final double START_MONEY = 300_000;

    public static void main(String[] args) {
        PurpleThing purpleThing = new PurpleThing();
        purpleThing.setCommission(COMMISSION);
        purpleThing.setMaxDec(MAX_DEC);
        purpleThing.setMinTran(MIN_TRANS);
        purpleThing.setRangeToPay(RANGE_TO_PAY);
        purpleThing.setRangeToSold(RANGE_TO_SOLD);
        purpleThing.setSoldPercent(SOLD_PERCENT);
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "incoming/kostyan.txt");

        //String result = new BufferedReader(new InputStreamReader(stream))
        //        .lines().collect(Collectors.joining("\n"));

        List<String> collect = new BufferedReader(new InputStreamReader(stream))
                .lines().collect(Collectors.toList());
        List<Double> cources = new ArrayList<>();
        for (String toDouble : collect) {
            cources.add(Double.parseDouble(toDouble.substring(25, 35)));
        }


        WhatIfAnalysis analysis = new WhatIfAnalysis(purpleThing);
        double doIt = analysis.doIt(0.001D, 0.0011D, 2,
                WhatIfAnalysisParameter.RANGE_TO_PAY, cources);
        System.out.println("Good Value: " + doIt);


    }

}
