package com.pegoopik.speculation.prototype;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class PurpleThing {

    private double soldPercent;
    private double rangeToPay;
    private double rangeToSold;
    private double maxDec;
    private double minTran;
    private double commission;
    
}
