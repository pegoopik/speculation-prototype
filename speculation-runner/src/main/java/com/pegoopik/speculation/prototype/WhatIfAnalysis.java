package com.pegoopik.speculation.prototype;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class WhatIfAnalysis {

    private static final double START_MONEY = 300_000;

    private final PurpleThing purpleThing;

    private final boolean autoAnalysis;

    public WhatIfAnalysis(PurpleThing purpleThing) {
        this.purpleThing = purpleThing;
        this.autoAnalysis = false;
    }

    public WhatIfAnalysis(PurpleThing purpleThing, boolean autoAnalisys) {
        this.purpleThing = purpleThing;
        this.autoAnalysis = autoAnalisys;
    }

    public double doIt(double from, double to, int count, WhatIfAnalysisParameter parameter, List<Double> cources) {
        double profit = -12345666;
        return doIt(from, to, count, parameter, cources, profit).getGoodValue();
    }

    @Getter
    @Setter
    public  static class WhatIfAnalysisResult {
        private Double profit;
        private Double goodValue;
    }

    public WhatIfAnalysisResult doIt(double from, double to, int count, WhatIfAnalysisParameter parameter, Iterable<Double> cources, double profit) {
        double goodValue = -12345777;
        PurpleAlgorithm lastPurpleAlgorithm = null;
        double lastCourse = 0D;
        String informMessageTotal = "";
        boolean needUpdateGoodValue = false;
        for (double i = from; i < to; i += (to - from) / count) {
            if (parameter == WhatIfAnalysisParameter.MAX_DEC) {
                purpleThing.setMaxDec(i);
            } else if (parameter == WhatIfAnalysisParameter.MIN_TRANS) {
                purpleThing.setMinTran(i);
            } else if (parameter == WhatIfAnalysisParameter.RANGE_TO_SOLD) {
                purpleThing.setRangeToSold(i);
            } else if (parameter == WhatIfAnalysisParameter.RANGE_TO_PAY) {
                purpleThing.setRangeToPay(i);
            } else if (parameter == WhatIfAnalysisParameter.SOLD_PERCENT) {
                purpleThing.setSoldPercent(i);
            }
            PurpleAlgorithm purpleAlgorithm = new PurpleAlgorithm(purpleThing, START_MONEY, autoAnalysis);
            lastPurpleAlgorithm = purpleAlgorithm;
            double payCount = 0D;
            for(Double cource : cources) {
                payCount = purpleAlgorithm.step(cource);
                lastCourse = cource;
            }
            double totalMoney = purpleAlgorithm.getMoneyATotal(lastCourse);
            if (totalMoney > profit) {
                needUpdateGoodValue = true;
                profit = totalMoney;
                goodValue = i;
                informMessageTotal = String.format("Чёкаво в итоге имеем aMoneyTotal: %s, bMoneyTotal: %s, коммиссия: %s, итераций: %s",
                        lastPurpleAlgorithm.getMoneyATotal(lastCourse),
                        lastPurpleAlgorithm.getMoneyBTotal(lastCourse),
                        lastPurpleAlgorithm.getPurpleThing().getCommission(),
                        payCount)
                        + "\n" + lastPurpleAlgorithm.getPurpleThing();
                System.out.println("newweat value" + informMessageTotal);
            }
        }
        System.out.println(informMessageTotal);
        WhatIfAnalysisResult result = new WhatIfAnalysisResult();
        if (needUpdateGoodValue) {
            result.setGoodValue(goodValue);
            result.setProfit(profit);
        }
        return result;
    }

}
