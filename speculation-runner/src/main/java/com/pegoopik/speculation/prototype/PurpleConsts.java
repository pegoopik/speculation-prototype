package com.pegoopik.speculation.prototype;

public class PurpleConsts {

    public static final double MIN_SUM_QUANT = 0.00000001D;

    public static final double SOLD_PERCENT = 0.9062499999999999D;
    public static final double RANGE_TO_PAY = 0.000973D;
//    public static final double RANGE_TO_PAY = 0.0018389999999999967D;
    public static final double RANGE_TO_SOLD = 0.00004410999999999984D;
    public static final double MAX_DEC = 0.0000004D;
    public static final double MIN_TRANS = 0.1D;
    public static final double COMMISSION = 0.00036D;



}
