package com.pegoopik.speculation.prototype;

public enum WhatIfAnalysisParameter {
    SOLD_PERCENT,
    RANGE_TO_SOLD,
    RANGE_TO_PAY,
    MAX_DEC,
    MIN_TRANS,
}
