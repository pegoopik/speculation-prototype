package com.pegoopik.speculation.prototype;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

import static com.pegoopik.speculation.prototype.PurpleConsts.MIN_SUM_QUANT;

@Setter
@Getter
public class PurpleAlgorithm {
    private double aMoney = 0;
    private double bMoney = 0;
    private double localMax = 0;
    private final PurpleThing purpleThing;
    private Map<UUID, Purchase> purchases = new HashMap<>();
    private int count = 0;

    private List<Double> moneyHistory = new ArrayList<>();

    private final boolean autoAnalysis;

    public void clear() {
        aMoney = 0;
        bMoney = 0;
        localMax = 0;
        //purpleThing = null;
        purchases = new HashMap<>();
        count = 0;
    }

    public PurpleAlgorithm(PurpleThing purpleThing, Double startMoney, boolean autoAnalysis) {
        this.purpleThing = purpleThing;
        this.aMoney = startMoney;
        this.autoAnalysis = autoAnalysis;
    }

    public PurpleAlgorithm(PurpleThing purpleThing, Double startMoney) {
        this.purpleThing = purpleThing;
        this.aMoney = startMoney;
        this.autoAnalysis = false;
    }


    public int step(double course) {
        //System.out.println("" + course + "\t" + localMax);
        this.localMax = localMax > course ? localMax : course;
        if (aMoney > 0) {
            //Если надо - покупаем
            if (localMax > course * (1 + purpleThing.getRangeToPay())) {
                count++;
                Purchase purchase = new Purchase();
                purchase.setCourse(course);
                purchase.setCourseToSold(purchase.getCourse() * (1 + purpleThing.getRangeToSold()));
                double sum = aMoney * purpleThing.getSoldPercent();
                //Если сумма платежа оставляет в остатке величину, меньшую минимальной транзакции
                //Тогда покупаем на всю сумму
                if ((getAMoney() - sum) <= getMoneyATotal(course) * purpleThing.getMinTran()) {
                    sum = getAMoney();
                }
                aMoney -= sum;
                double sumBWithCommission = (sum / course) * (1 - purpleThing.getCommission());
                bMoney +=  sumBWithCommission;
                prepareMoneys();
                purchase.setSum(sumBWithCommission);
                purchases.put(UUID.randomUUID(), purchase);
                localMax = course;
                if (!autoAnalysis) {
                    System.out.println(String.format("Покупка %s курс %s всего р %s $ %s ", sum, course, aMoney, bMoney));
                    System.out.println(String.format("aMoney: %s bMoney: %s count: %s", getMoneyATotal(course), getMoneyBTotal(course), count));
                }
                moneyHistory.add(getMoneyATotal(course));
            }
        }
        for(Map.Entry<UUID, Purchase> purchaseEntry : purchases.entrySet()) {
            purchaseEntry.getValue().setCourseToSold(
                    purchaseEntry.getValue().getCourseToSold() * (1 - purpleThing.getMaxDec()));
        }
        List<UUID> toDelete = new ArrayList<>();
        for (Map.Entry<UUID, Purchase> purchaseEntry : purchases.entrySet()) {
            Purchase purchase = purchaseEntry.getValue();
            //if (course > purchase.getCourse() * (1 + purpleThing.getRange())) {
            //GПродаём тута
            if (course > purchase.getCourseToSold()) {
                count++;
                aMoney += purchase.getSum() * course * (1 - purpleThing.getCommission());
                bMoney -= purchase.getSum();
                prepareMoneys();
                toDelete.add(purchaseEntry.getKey());
                if (!autoAnalysis) {
                    System.out.println(String.format("Продажа %s курс %s(%s/%s) всего р %s $ %s ",
                            purchase.getSum(), course, purchase.getCourse(), purchase.getCourseToSold(), aMoney, bMoney));
                    System.out.println(String.format("aMoney: %s bMoney: %s count: %s", getMoneyATotal(course), getMoneyBTotal(course), count));
                }
                moneyHistory.add(getMoneyATotal(course));
            }
        }
        for (UUID uuid : toDelete) {
            purchases.remove(uuid);
        }
        //TODO
        //System.out.println(course);
        return count;
    }

    private void prepareMoneys() {
        if (Math.abs(aMoney) <  MIN_SUM_QUANT)
            aMoney = 0;
        if (Math.abs(bMoney) <  MIN_SUM_QUANT)
            bMoney = 0;
    }


    public double getMoneyATotal(double course) {
        return aMoney + (bMoney * course);
    }

    public double getMoneyBTotal(double course) {
        return bMoney + (aMoney / course);
    }

}
