package com.pegoopik.speculation.prototype;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.pegoopik.speculation.prototype.PurpleConsts.COMMISSION;

public class BruteForceAnalysis {

    private static List<BruteForceResult> results = new ArrayList<>();
    private static final double START_MONEY = 300_000;

    @ToString
    @Getter
    @Setter
    public static class BruteForceResult {
        double profit;
        double maxDec;
        double minTrans;
        double rangeToPay;
        double rangeToSold;
        double soldPercent;
        int count;
    }

    public static void main(String[] args) {

        List<Double> cources = getKostyanValues();

        BrutForceCase maxDecCase = new BrutForceCase(
                0.000000004D, 0.00000004D, 10);
        BrutForceCase minTransCase = new BrutForceCase(
                0.002D, 0.04D, 10);
        BrutForceCase rangeToPayCase = new BrutForceCase(
                0.00002173D, 0.072573D, 10);
        BrutForceCase rangeToSoldCase = new BrutForceCase(
                0.00030410999999999984D, 0.0738410999999999984D, 10);
        BrutForceCase soldPercentCase = new BrutForceCase(
                0.15D, 0.9999D, 10);

        int totalCount = maxDecCase.getCount() * minTransCase.getCount() * rangeToPayCase.getCount() * rangeToSoldCase.getCount() * soldPercentCase.getCount();

        PurpleThing thing = new PurpleThing();
        /*
            private double soldPercent;
    private double rangeToPay;
    private double rangeToSold;
    private double maxDec;
    private double minTran;
    private double commission;
         */
        int i = 0;
        PurpleAlgorithm algorithm = new PurpleAlgorithm(thing, START_MONEY, true);
        thing.setCommission(COMMISSION);
        for (double i1 = maxDecCase.getStartValue(); i1 < maxDecCase.getEndValue(); i1 *= incBF(maxDecCase)) {
            System.out.println("1)--" + i1 + "" + LocalDateTime.now());
        for (double i2 = minTransCase.getStartValue(); i2 < minTransCase.getEndValue(); i2 *= incBF(minTransCase)) {
            System.out.println("2)----" + i2 + "" + LocalDateTime.now());
        for (double i3 = rangeToPayCase.getStartValue(); i3 < rangeToPayCase.getEndValue(); i3 *= incBF(rangeToPayCase)) {
            System.out.println("3)------" + i3 + "" + LocalDateTime.now());
        for (double i4 = rangeToSoldCase.getStartValue(); i4 < rangeToSoldCase.getEndValue(); i4 *= incBF(rangeToSoldCase)) {
            System.out.println("4)--------" + i + "/" + totalCount + " " + i4 + "" + LocalDateTime.now());
        for (double i5 = soldPercentCase.getStartValue(); i5 < soldPercentCase.getEndValue(); i5 *= incBF(soldPercentCase)) {
            i++;
            thing.setMaxDec(i1);
            thing.setMinTran(i2);
            thing.setRangeToPay(i3);
            thing.setRangeToSold(i4);
            thing.setSoldPercent(i5);
            //System.out.println(i1);
            algorithm.clear();
            algorithm.setAMoney(START_MONEY);
            int count = 0;
            double lastCourse = -12343D;
            for(double course : cources) {
                lastCourse = course;
                count = algorithm.step(course);
            }
            BruteForceResult bruteForceResult = new BruteForceResult();
            bruteForceResult.setCount(count);
            bruteForceResult.setMaxDec(thing.getMaxDec());
            bruteForceResult.setMinTrans(thing.getMinTran());
            bruteForceResult.setRangeToPay(thing.getRangeToPay());
            bruteForceResult.setRangeToSold(thing.getRangeToSold());
            bruteForceResult.setSoldPercent(thing.getSoldPercent());
            bruteForceResult.setProfit(algorithm.getMoneyATotal(lastCourse));
            results.add(bruteForceResult);
        }}}}}

        results.sort(Comparator.comparing(BruteForceResult::getProfit).reversed());
        int countOut = 10;
        int currentCount = 0;
        for (BruteForceResult result : results) {
            currentCount++;
            if (currentCount > countOut) return;
            System.out.println(result);
        }

    }

    private static double incBF(BrutForceCase brutForceCase) {
        return Math.pow(brutForceCase.getEndValue() / brutForceCase.getStartValue(), 1D / brutForceCase.getCount());
    }


    private static List<Double> getKostyanValues() {
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "incoming/kostyan.txt");

        //String result = new BufferedReader(new InputStreamReader(stream))
        //        .lines().collect(Collectors.joining("\n"));

        List<String> collect = new BufferedReader(new InputStreamReader(stream))
                .lines().collect(Collectors.toList());
        List<Double> cources = new ArrayList<>();
        for (String toDouble : collect) {
            cources.add(Double.parseDouble(toDouble.substring(25, 35)));
        }
        return cources;
    }

    @ToString
    @AllArgsConstructor
    @Getter
    @Setter
    public static class BrutForceCase {
        private double startValue;
        private double endValue;
        private int count;
    }

}
