package com.pegoopik.speculation.prototype;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Purchase {

    private double course;
    private double sum;
    private double courseToSold;

}
