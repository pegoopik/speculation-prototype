package com.pegoopik.speculation.prototype;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static com.pegoopik.speculation.prototype.PurpleConsts.*;

public class PurpleMainWhatIfAuto {

    private static double lastTotalMoney = -12345888D;

    private static final double START_MONEY = 300_000;

    private static final Iterable<Double> cources = getKostyanValues(false);

    public static void main(String[] args) {
        PurpleThing purpleThing = new PurpleThing();
        //В рамках одного прогона коммиссия есть константа
        purpleThing.setCommission(COMMISSION);
//        purpleThing.setMaxDec(MAX_DEC);
//        purpleThing.setMinTran(MIN_TRANS);
//        purpleThing.setRangeToPay(RANGE_TO_PAY);
//        purpleThing.setRangeToSold(RANGE_TO_SOLD);
//        purpleThing.setSoldPercent(SOLD_PERCENT);

        //Количество "перенастроек" алгоритма за один запуск
        int totalCount = 1;
        //Создаём параметры вотифанализа для каждого параметра алгоритма
        WhatIfCase maxDecCase = new WhatIfCase(WhatIfAnalysisParameter.MAX_DEC,
                0D, 0D, 1, true);
        WhatIfCase minTransCase = new WhatIfCase(WhatIfAnalysisParameter.MIN_TRANS,
                0D, 0D, 1, true);
        WhatIfCase rangeToPayCase = new WhatIfCase(WhatIfAnalysisParameter.RANGE_TO_PAY,
                0.004858481333333334D, 0.004858481333333334D, 1, true);
        WhatIfCase rangeToSoldCase = new WhatIfCase(WhatIfAnalysisParameter.RANGE_TO_SOLD,
                0.00030410999999999984D, 0.0738410999999999984D, 3, true);
        WhatIfCase soldPercentCase = new WhatIfCase(WhatIfAnalysisParameter.SOLD_PERCENT,
                1D, 1D, 1, true);

        Collection<Double> cources = getKostyanValues(false);

        //Установка начальных значений настроек алгоритма
        purpleThing.setMaxDec(maxDecCase.isEnabled() ? maxDecCase.getStartValue() : MAX_DEC);
        purpleThing.setMinTran(minTransCase.isEnabled() ? minTransCase.getStartValue() : MIN_TRANS);
        purpleThing.setRangeToPay(rangeToPayCase.isEnabled() ? rangeToPayCase.getStartValue() : RANGE_TO_PAY);
        purpleThing.setRangeToSold(rangeToSoldCase.isEnabled() ? rangeToSoldCase.getStartValue() : RANGE_TO_SOLD);
        purpleThing.setSoldPercent(soldPercentCase.isEnabled() ? soldPercentCase.getStartValue() : SOLD_PERCENT);

        for (int i=0; i<totalCount; i++) {
            WhatIfAnalysis analysis = new WhatIfAnalysis(purpleThing, true);
            System.out.println("PurpleMainWhatIfAuto try count : " + i + " , time is: " + LocalDateTime.now());
            purpleThing = processWhatIfCase(analysis, soldPercentCase, purpleThing);
            purpleThing = processWhatIfCase(analysis, rangeToPayCase, purpleThing);
            purpleThing = processWhatIfCase(analysis, rangeToSoldCase, purpleThing);
            purpleThing = processWhatIfCase(analysis, minTransCase, purpleThing);
            purpleThing = processWhatIfCase(analysis, maxDecCase, purpleThing);
        }

        PurpleAlgorithm purpleAlgorithm = new PurpleAlgorithm(purpleThing, START_MONEY);
        double lastCourse = 12346;
        for(Double cource : cources) {
            purpleAlgorithm.step(cource);
            lastCourse = cource;
        }
        double totalMoney = purpleAlgorithm.getMoneyATotal(lastCourse);

        System.out.println(totalMoney);
        System.out.println(purpleAlgorithm.getMoneyHistory());
        System.out.println(purpleThing);
    }

    private static PurpleThing processWhatIfCase(WhatIfAnalysis analysis, WhatIfCase whatIfCase, PurpleThing purpleThing) {
        System.out.println("!!![processWhatIfCase]: " + whatIfCase.toString() + "\n"
                + purpleThing + " time is: " + LocalDateTime.now().toString());
        if (!whatIfCase.isEnabled()) {
            return purpleThing;
        }
        //Запоминаем предыдущее "идеальное значение"
        double lastGoodValue = getValueFromPurpleThing(purpleThing, whatIfCase.getParameter());
        WhatIfAnalysis.WhatIfAnalysisResult whatIfAnalysisResult = analysis.doIt(
                whatIfCase.startValue, whatIfCase.endValue, whatIfCase.count, whatIfCase.parameter, cources, lastTotalMoney);
        if (whatIfAnalysisResult.getProfit() != null) { //TODO зарефачить, неочевидное условие, завязанное на null
            lastTotalMoney = whatIfAnalysisResult.getProfit();
            purpleThing = setGoodValueToPurpleThing(purpleThing, whatIfCase.getParameter(), whatIfAnalysisResult.getGoodValue());
            System.out.println("new good purple thing: " + purpleThing);
            return setGoodValueToPurpleThing(purpleThing, whatIfCase.getParameter(), whatIfAnalysisResult.getGoodValue());
        } else {
            //Возвращаем предыдущее "идеальное значение"
            return setGoodValueToPurpleThing(purpleThing, whatIfCase.getParameter(), lastGoodValue);
        }
    }

    private static double getValueFromPurpleThing(PurpleThing purpleThing, WhatIfAnalysisParameter parameter) {
        switch (parameter) {
            case MAX_DEC: return purpleThing.getMaxDec();
            case MIN_TRANS: return purpleThing.getMinTran();
            case RANGE_TO_PAY: return purpleThing.getRangeToPay();
            case RANGE_TO_SOLD: return purpleThing.getRangeToSold();
            case SOLD_PERCENT: return purpleThing.getSoldPercent();
            default: throw new RuntimeException();
        }
    }

    private static PurpleThing setGoodValueToPurpleThing(PurpleThing purpleThing, WhatIfAnalysisParameter parameter, double goodValue) {
        switch (parameter) {
            case MAX_DEC: purpleThing.setMaxDec(goodValue); return purpleThing;
            case MIN_TRANS: purpleThing.setMinTran(goodValue); return purpleThing;
            case RANGE_TO_PAY: purpleThing.setRangeToPay(goodValue); return purpleThing;
            case RANGE_TO_SOLD: purpleThing.setRangeToSold(goodValue); return purpleThing;
            case SOLD_PERCENT: purpleThing.setSoldPercent(goodValue); return purpleThing;
        }
        return purpleThing;
    }

    @ToString
    @AllArgsConstructor
    @Getter
    @Setter
    public static class WhatIfCase {
        private WhatIfAnalysisParameter parameter;
        private double startValue;
        private double endValue;
        private int count;
        private boolean enabled;
    }


    private static Collection<Double> getKostyanValues(boolean reverse) {
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "incoming/kostyan_2014 .txt");

        //String result = new BufferedReader(new InputStreamReader(stream))
        //        .lines().collect(Collectors.joining("\n"));

        List<String> collect = new BufferedReader(new InputStreamReader(stream))
                .lines().collect(Collectors.toList());
        if (!reverse) {
            return collect.stream().map(toDouble -> Double.parseDouble(toDouble.substring(25, 35))).collect(Collectors.toList());
        }
        List<Double> cources = new ArrayList<>();

        TreeMap<Integer, Double> result = new TreeMap<>();
        Integer i = 123456789;

        for (String toDouble : collect) {
            cources.add(Double.parseDouble(toDouble.substring(25, 35)));
            i--;
            result.put(i, Double.parseDouble(toDouble.substring(25, 35)));
        }

        //return cources;
        return result.values();
    }
}
