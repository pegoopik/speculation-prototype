package com.pegoopik.tools.gmap;

import lombok.Data;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GmapLogParser {

    public static void main(String[] args) {
        List<LineResult> lineResultList = new ArrayList<>();
        for(String line : getLinesFromResource()) {
            lineResultList.add(getLineResult(line));
        }
        for(LineResult lineResult : lineResultList) {
            System.out.println(toHumanString(lineResult));
        }
    }

    private static String toHumanString(LineResult lineResult) {
        String result = "";
        result += lineResult.getDateTime();
        result += "\t";
        result += getHumanWayPoints(lineResult);
        result += "\t";
        result += getHumanRoutes(lineResult);
        result += "\t";
        result += lineResult.getDistance();
        result += "\t";
        result += lineResult.getDuration();
        return result;
    }

    private static String getHumanWayPoints(LineResult lineResult) {
        String result = geoObjectToHumanStr(lineResult.getStart());
        for (GeoObject wayPoint : lineResult.getWaypoints()) {
            result += "," + geoObjectToHumanStr(wayPoint);
        }
        return result;
    }

    private static String getHumanRoutes(LineResult lineResult) {
        String result = geoObjectToHumanStr(lineResult.getStart());
        for(RouteLeg leg : lineResult.getRoute()) {
            result += "->" + geoObjectToHumanStr(leg.getEnd());
        }
        return result;
    }

    private static String geoObjectToHumanStr(GeoObject geoObject) {
        int length = 7;
        return "(" + geoObject.getLat().substring(0, length) + "; " + geoObject.getLon().substring(0, length) + ")";
    }

    private static List<String> getLinesFromResource() {
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "gmap/2020_06_29_logs.txt");

        //String result = new BufferedReader(new InputStreamReader(stream))
        //        .lines().collect(Collectors.joining("\n"));

        return new BufferedReader(new InputStreamReader(stream))
                .lines().collect(Collectors.toList());
//        List<Double> cources = new ArrayList<>();
//        for (String toDouble : collect) {
//            cources.add(Double.parseDouble(toDouble.substring(25, 35)));
//        }
//        return cources;
    }


    private static LineResult getLineResult(String line) {
        LineResult lineResult = new LineResult();
        lineResult.setRoute(new ArrayList<>());
        lineResult.setWaypoints(new ArrayList<>());
        String[] split1 = line.split("\t");
        String dateTime = split1[0];
        String other = split1[1];
        String[] split2 = other.split(";");
        String timeMsTemplate = split2[0].split(": ")[1];
        String startTemplate = split2[1].split(": ")[1];
        String waypointsTemplate = split2[2].split(": ")[1];
        String routeTemplate = split2[3].split(": ")[1];
        lineResult.setStart(parseGeoPoint(startTemplate));
        lineResult.setTimeMs(timeMsTemplate);
        lineResult.setDateTime(dateTime);
        lineResult.setWaypoints(parseWayPoints(waypointsTemplate));
        lineResult.setDuration(routeTemplate.split("Duration=")[1].split(",")[0]);
        lineResult.setDistance(routeTemplate.split("Meters=")[1].split(",")[0]);
        lineResult.setRoute(parseRoute(routeTemplate.split("legs=")[1]
                .replace("[", "")
                .replace("]", "")));
        return lineResult;
    }

    private static List<RouteLeg> parseRoute(String routeLegTemplate) {
        List<RouteLeg> routeLegs = new ArrayList<>();
        int i = 0;
        for (String routeLegStr : routeLegTemplate.split("RouteLeg")) {
            i++;
            if (i == 1) continue;
            routeLegs.add(parseRouteLeg(routeLegStr));
        }
        return routeLegs;
    }

    private static RouteLeg parseRouteLeg(String routeLegStr) {
        RouteLeg routeLeg = new RouteLeg();
        //(duration= distanceInMeters= startPoint= endPoint=
        String s1 = routeLegStr.split("duration=")[1].split(", distanceInMeters=")[0];
        String s2 = routeLegStr.split("distanceInMeters=")[1].split(", startPoint=")[0];
        String s3 = routeLegStr.split("startPoint=")[1].split(", endPoint=")[0];
        String s4 = routeLegStr.split("endPoint=")[1];
        routeLeg.setDuration(s1);
        routeLeg.setDistance(s2);
        routeLeg.setStart(parseGeoPoint(s3));
        routeLeg.setEnd(parseGeoPoint(s4));
        return routeLeg;
    }

    private static List<GeoObject> parseWayPoints(String waypointsTemplate) {
        List<GeoObject> wayPoints = new ArrayList<>();
        int i = 0;
        for (String geoPointStr : waypointsTemplate.split("GeoObject")) {
            i++;
            if (i == 1) continue;
            wayPoints.add(parseGeoPoint(geoPointStr));
        }
        return wayPoints;
    }

    private static GeoObject parseGeoPoint(String from) {
        GeoObject geoObject = new GeoObject();
        geoObject.setId(from.split("=")[1].split(",")[0]);
        geoObject.setLon(from.split("=")[2].split(",")[0]);
        geoObject.setLat(from.split("=")[3]
                .replace("]", "")
                .replace(")", "")
                .replace(",", "")
                .replace(" ", "")
        );
        return geoObject;
    }

    @Data
    public static class LineResult {
        private String dateTime;
        private String timeMs;
        private String duration;
        private String distance;
        private GeoObject start;
        private List<GeoObject> waypoints;
        private List<RouteLeg> route;
    }

    @Data
    public static class RouteLeg {
        private String duration;
        private String distance;
        private GeoObject start;
        private GeoObject end;
    }

    @Data
    public static class GeoObject {
        private String id;
        private String lon;
        private String lat;
    }

}
