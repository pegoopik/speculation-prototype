package com.pegoopik.speculation.service.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Api(description = "REST API для ручного обновления базы БИК")
@RequestMapping("v1/")
public class SystemController {


    /**
     * Скачивание БИК-ов с сайта ЦБ и сохранение в БД
     * Ручной запуск
     */
    @GetMapping("ping")
    public String merge() {
        return "Костян - дурак";
    }

}
