package com.pegoopik.speculation.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//@EntityScan(basePackageClasses = {BaseEntity.class})
@SpringBootApplication
@EnableSwagger2
public class SpringBootSpeculationApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSpeculationApp.class, args);
    }

}
